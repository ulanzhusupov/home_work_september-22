import 'dart:io';

void main(List<String> arguments) {
  print(task1());
  // task2();
  // print(task3());
  // print(task4());
  // print(task5());
}

/* Task 1
Напишите программу, которая запрашивает у пользователя
начало и конец диапазона целых чисел, а затем использует
цикл for для подсчета суммы всех чисел в этом диапазоне.*/
int task1() {
  stdout.write("Введите начало числа: ");
  int a = int.parse(stdin.readLineSync()!);

  stdout.write("Введите конец числа: ");
  int b = int.parse(stdin.readLineSync()!);

  if (a >= b) {
    print("Начало не может быть больше или равно конца");
    return 0;
  }

  int summ = 0;

  for (int i = a; i <= b; i++) {
    summ += i;
  }

  return summ;
}

/* Task 2
Запросите у пользователя год и определите,
является ли он високосным с использованием
конструкции if else и цикла.*/
void task2() {
  stdout.write("Введите год, чтобы узнать является ли год високосным: ");
  int year = int.parse(stdin.readLineSync()!);

  if (year % 4 == 0 || (year % 100 == 0 && year % 400 == 0)) {
    print("Год високосный");
  } else {
    print("Год не високосный");
  }
}

/* Task3
Запросите у пользователя два числа и найдите их
наименьшее общее кратное с использованием цикла
и конструкции if else.*/
int task3() {
  stdout.write("Введите первое число: ");
  int a = int.parse(stdin.readLineSync()!);

  stdout.write("Введите второе число: ");
  int b = int.parse(stdin.readLineSync()!);

  int bigNumber = a > b ? a : b;

  int nok = 0;

  for (int i = 2; i < bigNumber; i++) {
    int c = bigNumber * i;
    if (c % a == 0 && c % b == 0) {
      nok = c;
      return nok;
    }
  }

  return 0;
}

/* Task 4
Попросите пользователя ввести целое число,
а затем используйте цикл и конструкцию if else
для подсчета суммы его цифр.*/

int task4() {
  stdout.write("Введите целое число: ");
  int num = int.parse(stdin.readLineSync()!);
  int sum = 0;
  while (num > 0) {
    int digit = num % 10; // Получаем последнюю цифру числа
    sum += digit; // Добавляем цифру к сумме
    num ~/= 10; // Убираем последнюю цифру из числа
    print(num);
  }

  return sum;
}

/* Task 5
Запросите у пользователя слово или фразу и определите,
является ли оно палиндромом (читается одинаково с начала и с конца)
с использованием цикла и конструкции if else.*/
bool task5() {
  stdout.write("Введите слово: ");
  String word = stdin.readLineSync() ?? '';
  String reversed = word.split('').reversed.join('');

  return word == reversed;
}
